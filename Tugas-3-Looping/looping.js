// No. 1 Looping While

console.log("LOOPING PERTAMA");
var num = 1;
while(num <= 20){
	if(num % 2  == 0){
		console.log(num + " - I love coding");
	}
	num++;
}

console.log("LOOPING KEDUA");
var num = 20;
while(num > 0){
	if(num % 2  == 0){
		console.log(num + " - I will become a mobile developer");
	}
	num--;
}

console.log("\n");
// No. 2 Looping menggunakan for
console.log("OUTPUT");
for(var i = 1; i <=20; i++){
	if(i % 2  == 0){
		console.log(i + " - Berkualitas");
	}
	else {
		if(i % 3  == 0){
			console.log(i + " - I Love Coding");
		}
		else {
			console.log(i + " - Santai");
		}
	}
}

console.log("\n");
// No. 3 Membuat Persegi Panjang #
var i = 4;
while(i > 0){
	for(var j=1; j<=8; j++){
		process.stdout.write("#");
	}
	console.log("");
	i--
}

console.log("\n");
// No. 4 Membuat Tangga
for(var i=1; i<=7; i++){
	var x = 0;
	while(x < i){
		process.stdout.write("#");
		x++;
	}
	console.log("");
}

console.log("\n");
// No. 5 Membuat Papan Catur
for(var i=1; i<=8; i++){
	for(var j=1; j<=8; j++){
		if(i % 2 != 0){
			if(j % 2 == 0){
				process.stdout.write("#");
			}
			else {
				process.stdout.write(" ");
			}
		}
		else {
			if(j % 2 == 0){
				process.stdout.write(" ");
			}
			else {
				process.stdout.write("#");
			}
		}
	}
	console.log("");
}

