import React, { Component } from 'react';
import { Image, TouchableOpacity, View , StyleSheet, FlatList, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import data from './data.json'
import VideoItem from './Component/VideoItem'
import { FontAwesome5, MaterialCommunityIcons } from '@expo/vector-icons'; 


export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                  <Image source={require('./Images/logo.png')} style={{width:98, height:22}}></Image>
                  <View style={styles.rightNav}>
                    <TouchableOpacity>
                      <Icon style={styles.navItem} name="search" size={25}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <Icon style={styles.navItem} name="account-circle" size={25}/>
                    </TouchableOpacity>
                  </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    navBar: {
      height: 55,
      backgroundColor: 'white',
      elevation: 3,
      paddingHorizontal: 15,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    rightNav: {
      flexDirection: 'row',
    },
    navItem: {
      marginLeft: 25
    },
    body: {
      flex: 1
    },
  });