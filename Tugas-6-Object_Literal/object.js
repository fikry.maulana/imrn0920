//Soal No. 1 (Array to Object)
console.log("===== Soal No. 1 (Array to Object) =====");
function arrayToObject(arr) {
    // Code di sini 
    if(arr.length > 0){
        var now = new Date();
        var thisYear = now.getFullYear();
        var umur = "";
        var people = {}
        var num = 1;
        for(var x = 0; x < arr.length; x++){
            num = num + x;
            if(typeof arr[x][3] !== 'undefined' && arr[x][3] < thisYear){
                umur = thisYear - arr[x][3];
            }
            else {
                umur = "Invalid birth year"
            }
            people[num + ". " + arr[x][0]+ " " +arr[x][1]] = {
                firstName: arr[x][0],
                lastName: arr[x][1],
                gender: arr[x][2],
                age: umur

            }
        }
    }
    else {
        var people = "";
    }
    console.log(people);
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
// Error case 
arrayToObject([]) // ""


//Soal No. 2 (Shopping Time)
console.log("")
console.log("===== Soal No. 2 (Shopping Time) =====");
function shoppingTime(memberId, money) {
    var result = "";
    var items = [
        {
            barang : "Sepatu Stacattu",
            harga : 1500000
        },
        {
            barang : "Baju Zoro",
            harga : 500000
        },
        {
            barang : "Baju H&N",
            harga : 250000
        },
        {
            barang : "Sweater Uniklooh",
            harga : 175000
        },
        {
            barang : "Casing Handphone",
            harga : 50000
        }
    ]
    var keranjang = [];

    if(typeof memberId !== 'undefined' && memberId != ''){
        if(money >= 50000){ 
            var sisa = money;
            for(var i = 0; i < items.length; i++){
                if(items[i].harga <= sisa){
                    keranjang.push(items[i].barang);
                    sisa -= items[i].harga;
                }
            }
            var result = {
                memberId : memberId,
                money : money,
                listPurchased : keranjang,
                changeMoney : sisa
            };
        }
        else {
            result = "Mohon maaf, uang tidak cukup";
        }

    }
    else {
        result = "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    return result;
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


//Soal No. 3 (Naik Angkot)
console.log("")
console.log("Soal No. 3 (Naik Angkot)");
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var result = [];
  for(var i = 0; i < arrPenumpang.length; i++){
    var start = rute.indexOf(arrPenumpang[i][1]);
    var end = rute.indexOf(arrPenumpang[i][2]);
    var ongkos = (end - start) * 2000;
    var data = {
        penumpang: arrPenumpang[i][0], 
        naikDari: arrPenumpang[i][1], 
        tujuan: arrPenumpang[i][2], 
        bayar: ongkos
    }
    result.push(data);
  }
  return result;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]