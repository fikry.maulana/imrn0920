// No 1
console.log("===== No 1 Teriak =====");
function teriak(){
	return "Halo Sanbers!";
}
console.log(teriak()) // "Halo Sanbers!" 

// No 2
console.log("");
console.log("===== No 2 Kalikan =====");
function kalikan(a, b){
	return a*b;
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

// No 3
console.log("");
console.log("===== No 3 Introduce =====");
function introduce(nama, umur, alamat, hobby){
	var intro = "Nama saya "+ nama +", umur saya "+ umur +" tahun, alamat saya di "+ alamat +", dan saya punya hobby yaitu "+ hobby +"!";
	return intro;
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
console.log("");