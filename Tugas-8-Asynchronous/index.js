// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

console.log("")
console.log("===== Soal No. 1 (Callback Baca Buku) =====");
//Soal No. 1 (Callback Baca Buku)
// Tulis code untuk memanggil function readBooks di sini

var i = 0;
let baca = (waktu, buku) => {
	if(i < books.length){
		readBooks(waktu, buku[i], (sisa) =>{
			i += 1;
			baca(sisa, buku);
		})
	}
};

baca(10000, books);
