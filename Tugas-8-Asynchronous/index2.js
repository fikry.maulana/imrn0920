var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

var time = 10000;
var sisa = 10000;
var i = 0;
var status = 1;
const read = setInterval(() => {
	if(i < books.length && sisa == time){
		let reads = readBooksPromise(sisa, books[i]);
		reads.then(wkt => { 
		    sisa = wkt;
		    i +=1;
		}); 
	}

	time -=1000;
	if(time == 0 ){
		clearInterval(read);
	}

}, 1000);