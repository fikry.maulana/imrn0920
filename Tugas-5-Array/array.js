
//Soal No. 1 (Range)
console.log("===== Soal No. 1 (Range) =====");
function range(startNum, finishNum){
	var range = [];
	if(typeof startNum !== 'undefined' && typeof finishNum !== 'undefined'){
		if(startNum == finishNum){
			range.push(startNum);
		}
		else if(startNum < finishNum) {
			for(var i=startNum; i<=finishNum; i++){
				range.push(i);
			}
		}
		else if(startNum > finishNum) {
			for(var i=startNum; i>=finishNum; i--){
				range.push(i);
			}
		}
	}
	else {
		var range = -1;
	}
	return range;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

//Soal No. 2 (Range with Step)
console.log("")
console.log("===== Soal No. 2 (Range with Step) =====");
function rangeWithStep(startNum, finishNum, step) {
	var range = [];
	if(startNum < finishNum) {
		for(var i=startNum; i<=finishNum; i+=step){
			range.push(i);
		}
	}
	else if(startNum > finishNum) {
		for(var i=startNum; i>=finishNum; i-=step){
			range.push(i);
		}
	}
	return range;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal No. 3 (Sum of Range)
console.log("")
console.log("===== Soal No. 3 (Sum of Range) =====");
function sum(startNum, finishNum, step){
	var total = 0;
	var data = [];
	if(typeof step !== 'undefined'){
		data = rangeWithStep(startNum, finishNum, step);
	}
	else {
		if(typeof finishNum !== 'undefined'){
			data = range(startNum, finishNum);
		}
		else {
			if(typeof startNum !== 'undefined'){
				data.push(startNum);
			}
			else {
				data.push(0);
			}
		}
	}

	for(var i=0; i < data.length; i++){
		total += data[i];
	}
	return total;
}


// Code di sini
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//Soal No. 4 (Array Multidimensi)
console.log("");
console.log("===== Soal No. 4 (Array Multidimensi) =====");
//contoh input
var input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(data){
	var user = "";
	for (var i=0; i < data.length; i++) {
		user += "Nomor ID: "+ data[i][0] +"\n";
		user += "Nama Lengkap: "+ data[i][1] +"\n";
		user += "TTL: "+ data[i][2] +" "+ data[i][3] +"\n";
		user += "Hobi: "+ data[i][4] +"\n";
		user += "\n";
	}
	return user;
}
console.log(dataHandling(input));

//Soal No. 5 (Balik Kata)
console.log("");
console.log("===== Soal No. 5 (Balik Kata) =====");
function balikKata(kata){
	var kataBaru = "";
	var i = kata.length;
	while(i > 0){
		kataBaru += kata.charAt(i-1);
		i--;
	}
	return kataBaru;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


//Soal No. 6 (Metode Array)
console.log("");
console.log("===== Soal No. 6 (Metode Array) =====");
function dataHandling2(data){
	var idx = data.length;
	data.splice(1, 1, data[1] + "Elsharawy");
	data.splice(2, 1, "Provinsi " + data[2]);
	data.splice(idx-1, 1, "Pria", "SMA Internasional Metro");
	console.log(data);

	var bulan = data[3].split("/");
	switch(parseInt(bulan[1])){
		case 1:
			console.log("Januari");
		break;
		case 2:
			console.log("Februari");
		break;
		case 3:
			console.log("Maret");
		break;
		case 4:
			console.log("April");
		break;
		case 5:
			console.log("Mei");
		break;
		case 6:
			console.log("Juni");
		break;
		case 7:
			console.log("Juli");
		break;
		case 8:
			console.log("Agustus");
		break;
		case 9:
			console.log("September");
		break;
		case 10:
			console.log("Oktober");
		break;
		case 11:
			console.log("Nopember");
		break;
		case 12:
			console.log("Desember");
		break;
		default:
			console.log("Januari");
		break;
	}

	// sort array descending
	var bulanDesc = bulan.slice();
	bulanDesc.sort(function (value1, value2) { return value2 - value1 } ) ;
	console.log(bulanDesc);

	//join 
	console.log(bulan.join("-"));

	//slice nama
	console.log(data[1].slice(0, 15));

}


var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
